FROM python:3.8 as base

# Install pipenv and compilation dependencies
RUN apt-get update && apt-get install -y --no-install-recommends build-essential jq curl

# Create and switch to a new user
RUN useradd --create-home python
USER python
WORKDIR /home/python

# Setup env
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONFAULTHANDLER 1
ENV PATH="${PATH}:/home/python/.local/bin"

# Install python dependencies
FROM base AS python-deps

# Install pipenv
RUN pip install --user pipenv

# Install python dependencies in /.venv
ADD Pipfile .
ADD Pipfile.lock .
RUN PIPENV_VENV_IN_PROJECT=1 pipenv install --deploy

FROM base AS runtime

# Copy virtual env from python-deps stage
COPY --from=python-deps /home/python/.venv /home/python/.venv
ENV PATH="/home/python/.venv/bin:$PATH"

# Prepare mountpoint for results
USER root
RUN mkdir /srv/data
RUN chown python:python /srv/data

# Install application into container
USER python
ADD --chown=python:python . /home/python/code
WORKDIR /home/python/code
RUN pip install -e .