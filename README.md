# Parsoid read view benchmark

## Dependencies

- python (>3.8)
- pipenv
- redis

or

- docker
- docker-compose

## Installation

Create a new virtual environment with:

```bash
pipenv install
pipenv install --dev
```

For a single page spawn a new python REPL:

```bash
pipenv shell
python
```

and run:

```python
from benchmark import Benchmark
b = Benchmark("en.wikipedia.org", "Dog")
results = b.run()
```

## Run benchmark

```bash
# Run message broker and workers
docker-compose up -d

# Build input dataset and run tasks
docker-compose run worker bash
parsoidbenchmark-dataset /srv/data/input
parsoidbenchmark-tasks /srv/data/output /srv/data/input/dataset.json
```

## Testing

To run the tests:

```bash
cd tests
python -m unittest discover -t ..
```
