import responses
import unittest

from bs4 import BeautifulSoup
from parsoidbenchmark import Benchmark


class TestStripping(unittest.TestCase):
    def test_stripping_datamw(self):
        b = Benchmark("en.wikipedia.org", "Dog")
        b.getLegacy()
        b.responseContent = """
            <html>
                <body>
                <div data-mw="example">
                    <p>Foo</p><span data-mw="example">Bar</span>
                </div>
                </body>
            </html>
        """
        expectedHTML = """
            <html>
                <body>
                <div>
                    <p>Foo</p><span>Bar</span>
                </div>
                </body>
            </html>
        """
        b.strip(["stripDatamw"])
        self.assertEqual(str(b.html), str(BeautifulSoup(expectedHTML, "lxml")))

    def test_stripping_typeof(self):
        b = Benchmark("en.wikipedia.org", "Dog")
        b.getLegacy()
        b.responseContent = """
            <html>
                <body>
                <div typeof="example">
                    <p>Foo</p><span typeof="example">Bar</span>
                </div>
                </body>
            </html>
        """
        expectedHTML = """
            <html>
                <body>
                <div>
                    <p>Foo</p><span>Bar</span>
                </div>
                </body>
            </html>
        """
        b.strip(["stripTypeof"])
        self.assertEqual(str(b.html), str(BeautifulSoup(expectedHTML, "lxml")))

    def test_stripping_rel(self):
        b = Benchmark("en.wikipedia.org", "Dog")
        b.getLegacy()
        b.responseContent = """
            <html>
                <body>
                <div rel="mw:FooBar">
                    <p>Foo</p><span rel="FooBar">Bar</span>
                </div>
                </body>
            </html>
        """
        expectedHTML = """
            <html>
                <body>
                <div>
                    <p>Foo</p><span>Bar</span>
                </div>
                </body>
            </html>
        """
        b.strip(["stripRel"])
        self.assertEqual(str(b.html), str(BeautifulSoup(expectedHTML, "lxml")))

    def test_stripping_mwentity(self):
        b = Benchmark("en.wikipedia.org", "Dog")
        b.getLegacy()
        b.responseContent = """
            <html>
                <body>
                <div rel="mw:FooBar">
                    <p>Foo</p><span typeof="mw:Entity">Stripped</span><span typeof="mw:Entity" rel="asdf">Bar</span></div>
                </body>
            </html>
        """
        expectedHTML = """
            <html>
                <body>
                <div rel="mw:FooBar">
                    <p>Foo</p>Stripped<span typeof="mw:Entity" rel="asdf">Bar</span></div>
                </body>
            </html>
        """
        b.strip(["stripSpanMWEntity"])
        self.assertEqual(str(b.html), str(BeautifulSoup(expectedHTML, "lxml")))

    def test_stripping_sections(self):
        b = Benchmark("en.wikipedia.org", "Dog")
        b.getLegacy()
        b.responseContent = """
            <html>
                <body>
                <p><section>SECTION</section></p>
                <div rel="mw:FooBar">
                    <p>Foo</p><span>Stripped</span><span>Bar</span></div>
                </body>
            </html>
        """
        expectedHTML = """
            <html>
                <body>
                <p>SECTION</p>
                <div rel="mw:FooBar">
                    <p>Foo</p><span>Stripped</span><span>Bar</span></div>
                </body>
            </html>
        """
        b.strip(["stripSections"])
        self.assertEqual(str(b.html), str(BeautifulSoup(expectedHTML, "lxml")))

    def test_stripping_spanabout(self):
        b = Benchmark("en.wikipedia.org", "Dog")
        b.getLegacy()
        b.responseContent = """
            <html>
                <body>
                <div about="#mwt678" rel="mw:FooBar">
                    <p>Foo</p><span about="#mwt1234">Bar</span></div>
                </body>
            </html>
        """
        expectedHTML = """
            <html>
                <body>
                <div about="#mwt678" rel="mw:FooBar">
                    <p>Foo</p>Bar</div>
                </body>
            </html>
        """
        b.strip(["stripSpanAbout"])
        self.assertEqual(str(b.html), str(BeautifulSoup(expectedHTML, "lxml")))

    def test_stripping_parsoidIDs(self):
        b = Benchmark("en.wikipedia.org", "Dog")
        b.getLegacy()
        b.responseContent = """
            <html>
                <body>
                <div rel="mw:FooBar">
                    <p>Foo</p><a href="#" id="mwABCD123">Bar</a></div>
                </body>
            </html>
        """
        expectedHTML = """
            <html>
                <body>
                <div rel="mw:FooBar">
                    <p>Foo</p><a href="#">Bar</a></div>
                </body>
            </html>
        """
        b.strip(["stripParsoidIds"])
        self.assertEqual(str(b.html), str(BeautifulSoup(expectedHTML, "lxml")))

    @responses.activate
    def test_full(self):

        responseContent = """
            <html>
                <body data-mw="asdf" rel="mw:asdf">
                <p><section>SECTION</section></p>
                <span typeof="example" rel="mw:FooBar">Example</span>
                <div rel="mw:FooBar" id="mwABC123">
                    <p><span about="#mwt1234">BAR</span></p>
                    <p><span typeof="mw:Entity">FOOBAR</span></p>
                    <p>Foo</p><a href="#" id="mwABCD123">Bar</a></div>
                </body>
            </html>
        """
        responses.add(
            responses.PassthroughResponse(
                responses.GET,
                "https://en.wikipedia.org/w/api.php?action=parse&page=Dog&format=json&disablelimitreport=true&redirects=true",
            )
        )
        resp = responses.Response(
            method="GET",
            body=responseContent,
            status=200,
            url="https://en.wikipedia.org/api/rest_v1/page/html/Dog",
        )
        responses.add(resp)

        b = Benchmark("en.wikipedia.org", "Dog")
        b.getLegacy()
        expectedHTML = """
            <html>
                <body>
                <p>SECTION</p>
                <span>Example</span>
                <div>
                    <p>BAR</p>
                    <p>FOOBAR</p>
                    <p>Foo</p><a href="#">Bar</a></div>
                </body>
            </html>
        """
        b.run()
        self.assertEqual(str(b.html), str(BeautifulSoup(expectedHTML, "lxml")))

    def test_run(self):
        b = Benchmark("en.wikipedia.org", "wework")
        results = b.run()
