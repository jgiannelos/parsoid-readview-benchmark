import json
import re
import zlib
from functools import partial
from urllib.parse import quote_plus, urljoin

import requests
from bs4 import BeautifulSoup, Comment


class Benchmark:

    ALL_RULES = {
        "stripDatamw": {"attribute": "data-mw", "matcher": None},
        "stripSections": {
            "attribute": None,
            "tag": "section",
            "matcher": lambda attr, tag: True,
            "replacer": lambda x: x.unwrap(),
        },
        "stripRel": {
            "attribute": "rel",
            "matcher": None,
        },
        "stripSpanMWEntity": {
            "attribute": "typeof",
            "tag": "span",
            "matcher": lambda attr, tag: (
                attr == "mw:Entity"
                and set(tag.attrs.keys()).issubset(set(["about", "typeof"]))
            ),
            "replacer": lambda x: x.unwrap(),
        },
        "stripSpanAbout": {
            "attribute": "about",
            "tag": "span",
            "matcher": lambda attr, tag: set(tag.attrs.keys()) == set(["about"]),
            "replacer": lambda x: x.unwrap(),
        },
        "stripAboutIds": {
            "attribute": "about",
            "matcher": None,
        },
        "stripTypeof": {"attribute": "typeof", "matcher": None},
        "stripParsoidIds": {
            "attribute": "id",
            "matcher": lambda attr, tag: bool(re.match("mw[a-zA-Z0-9_-]*", attr)),
        },
    }

    def __init__(self, domain, title, stripEnabled=True):
        self.domain = domain
        self.title = title
        self.stripEnabled = stripEnabled

    @staticmethod
    def tagsMatchingRules(tag, attr, matcher, node):
        if tag is not None:
            if node.name != tag:
                return False
            if attr is None:
                return matcher(None, node)
        if node.has_attr(attr):
            value = node[attr]
            if matcher:
                return matcher(value, node)
            return True
        return False

    def getPageURL(self):
        baseURL = f"https://{self.domain}/api/rest_v1/page/html/"
        return urljoin(baseURL, quote_plus(self.title))

    def getPage(self):
        res = requests.get(self.getPageURL())
        res.raise_for_status()
        page = res.content.decode()
        self.responseContent = page

    def getLegacy(self):
        title = quote_plus(self.title)
        url = f"https://{self.domain}/w/api.php?action=parse&page={title}&format=json&disablelimitreport=true&redirects=true"
        res = requests.get(url)
        res.raise_for_status()
        data = res.json()

        html = BeautifulSoup(data["parse"]["text"]["*"], "lxml")

        comments = html.find_all(string=lambda x: isinstance(x, Comment))
        for comment in comments:
            comment.extract()

        # Strip TOC
        matches = html.find_all(class_="toc")
        for match in matches:
            match.decompose()

        # Strip edit section
        matches = html.find_all(class_="mw-editsection")
        for match in matches:
            match.decompose()

        self.legacyParserHTML = html
        self.legacyParserSize = len(zlib.compress(str(self.legacyParserHTML).encode()))

    def getHTML(self):
        html = BeautifulSoup(self.responseContent, "lxml")
        comments = html.find_all(string=lambda x: isinstance(x, Comment))
        for comment in comments:
            comment.extract()
        self.html = html

    def findMatches(self, rule):
        attr = rule["attribute"]
        matcher = rule["matcher"]
        tag = rule.get("tag", None)
        matcherRule = partial(self.tagsMatchingRules, tag, attr, matcher)
        tags = self.html.find_all(matcherRule)
        matches = {
            "attribute": attr,
            "tags": tags,
            "replacer": rule.get("replacer", None),
        }
        return matches

    def stripHTML(self, matches):
        attribute = matches["attribute"]
        for tag in matches["tags"]:
            if matches["replacer"] is not None:
                matches["replacer"](tag)
            else:
                del tag[attribute]

    def getCompressedSize(self):
        compressed = zlib.compress(str(self.html.body).encode())
        return len(compressed)

    def strip(self, rules):
        self.getHTML()
        originalSize = self.getCompressedSize()

        for rule in rules:
            matches = self.findMatches(self.ALL_RULES[rule])
            self.stripHTML(matches)

        strippedSize = self.getCompressedSize()
        return {
            "originalSize": originalSize,
            "strippedSize": strippedSize,
            "legacyParserSize": self.legacyParserSize,
            "parsoidDiffRatio": 100 * (strippedSize - originalSize) / originalSize,
            "legacyDiffRatio": 100
            * (strippedSize - self.legacyParserSize)
            / self.legacyParserSize,
        }

    def run(self):
        results = {}
        self.getPage()
        self.getHTML()
        self.getLegacy()

        if self.stripEnabled:
            # Run each stripping rule seperately
            for rule in self.ALL_RULES:
                results[rule] = self.strip([rule])

            # Run stripping with all rules combined
            results["full"] = self.strip(self.ALL_RULES.keys())

            return results

        return {
            "originalSize": self.getCompressedSize(),
            "legacyParserSize": self.legacyParserSize,
        }


def run_benchmark(domain, title, output, stripEnabled=True):
    b = Benchmark(domain, title, stripEnabled)
    results = json.dumps(b.run())
    with open(f"{output}/{domain}-{title}.json", "w") as f:
        f.write(results)
