import json
import operator

import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns


def getDataframe(
    datasetPath="../data/output-metrics.csv", viewsPath="../data/views.json"
):
    df = pd.read_csv(datasetPath)
    with open(viewsPath, "r") as f:
        views = json.load(f)

    def process(row):
        try:
            return views[row["domain"]][row["title"]]
        except:
            print(f'{row["domain"]} {row["title"]}')

    pageviews = df.apply(process, axis=1)
    metrics = df.loc[:, ~df.columns.isin(["domain", "title"])]
    pages = df.loc[:, df.columns.isin(["domain", "title"])]
    metrics = metrics.mul(pageviews, axis=0)

    return pd.concat([pages, metrics], axis=1)


def getDiffColumns():
    columns = [
        ("stripDatamw", "stripDatamw.strippedSize", "stripDatamw.originalSize"),
        ("stripSections", "stripSections.strippedSize", "stripSections.originalSize"),
        ("stripRel", "stripRel.strippedSize", "stripRel.originalSize"),
        ("stripTypeof", "stripTypeof.strippedSize", "stripTypeof.originalSize"),
        (
            "stripSpanMWEntity",
            "stripSpanMWEntity.strippedSize",
            "stripSpanMWEntity.originalSize",
        ),
        (
            "stripSpanAbout",
            "stripSpanAbout.strippedSize",
            "stripSpanAbout.originalSize",
        ),
        ("stripAboutIds", "stripAboutIds.strippedSize", "stripAboutIds.originalSize"),
        (
            "stripParsoidIds",
            "stripParsoidIds.strippedSize",
            "stripParsoidIds.originalSize",
        ),
        ("stripFull", "full.strippedSize", "full.originalSize"),
        ("stripFullLegacy", "full.strippedSize", "full.legacyParserSize"),
    ]
    return columns


def getDiffsStats(df):
    columns = getDiffColumns()
    names = map(operator.itemgetter(0), columns)
    stats = pd.DataFrame()
    for name in names:
        stats[name] = df[name].describe()
    return stats


def getDiffs(df):
    columns = getDiffColumns()
    for (name, columnA, columnB) in columns:
        df[name] = (df[columnA] - df[columnB]) * 100 / df[columnB]

    return df


def showColumnHistQuantile(df, column, lowThreshold, upperThreshold):
    filtered = df[
        (df[column[0]] <= df[column[0]].quantile(upperThreshold))
        & (df[column[0]] >= df[column[0]].quantile(lowThreshold))
    ]
    plot = sns.histplot(filtered[column[0]], stat="percent")
    plot.set_xlabel(f"Page size difference %: {column[1]} VS {column[2]}")
    plot.set_ylabel("Percentage of pages %")
    plt.show()
