import json
import pandas
from pathlib import Path

import click
import re


@click.command()
@click.argument("inputpath", required=True)
@click.argument("outputpath", required=True)
def main(inputpath, outputpath):
    files = Path(inputpath).glob("*")
    pattern = r"(\w+\.\w+\.\w+)\-(.*)\.json"
    df = pandas.DataFrame()

    for file in files:
        matches = re.match(pattern, file.name)
        domain = matches.group(1)
        title = matches.group(2)

        with open(file, "r") as f:
            data = json.load(f)

        click.echo(f"Processing domain={domain} title={title}")
        data["domain"] = domain
        data["title"] = title
        df = pandas.concat([df, pandas.json_normalize(data)])

    df.to_csv(outputpath, index=False)


if __name__ == "__main__":
    main()
