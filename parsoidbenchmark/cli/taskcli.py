import json
import os

import click
from redis import Redis
from rq import Queue

from parsoidbenchmark.benchmark import run_benchmark


@click.command()
@click.argument("output", required=True)
@click.argument("dataset", required=True)
@click.option("--strip-enabled/--strip-disabled", is_flag=True, default=True)
def main(output, dataset, strip_enabled):
    q = Queue(
        connection=Redis.from_url(
            os.environ.get("BENCHMARK_REDIS_URL", "redis://localhost:6379/0")
        )
    )
    os.makedirs(output, exist_ok=True)

    with open(dataset, "r") as f:
        data = json.load(f)

    for domain in data:
        for title in data[domain]:
            q.enqueue(run_benchmark, domain, title, output, strip_enabled)
            click.echo(f"Enqueued task for: {domain}:{title}")


if __name__ == "__main__":
    main()
