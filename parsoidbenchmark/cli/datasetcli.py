from collections import defaultdict
import json
import os

import click
import requests

query = "https://wikimedia.org/api/rest_v1/metrics/pageviews/top/{domain}/all-access/{year}/{month}/all-days"
year = "2022"
month = "03"
domains = [
    "en.wikipedia.org",
    "ja.wikipedia.org",
    "fr.wikipedia.org",
    "es.wikipedia.org",
    "ru.wikipedia.org",
    "de.wikipedia.org",
    "it.wikipedia.org",
    "pt.wikipedia.org",
]


@click.command()
@click.argument("path", required=True)
def main(path):
    os.makedirs(path, exist_ok=True)
    headers = {
        "User-Agent": "content-transform-requests/0.1 (https://www.mediawiki.org/wiki/Content_Transform_Team; jgiannelos@wikimedia.org)"
    }
    views = {}
    dataset = {}

    for domain in domains:
        res = requests.get(
            query.format(domain=domain, year=year, month=month), headers=headers
        )
        res.raise_for_status()
        data = res.json()
        domain_articles = []
        domain_views = {}

        for entry in data["items"][0]["articles"]:
            domain_articles.append(entry["article"])
            domain_views[entry["article"]] = entry["views"]

        dataset[domain] = domain_articles
        views[domain] = domain_views

    with open(f"{path}/dataset.json", "w") as f:
        json.dump(dataset, f)
    with open(f"{path}/views.json", "w") as f:
        json.dump(views, f)


if __name__ == "__main__":
    main()
