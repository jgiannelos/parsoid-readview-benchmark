from collections import defaultdict

import bs4
import pandas
import requests


class HTMLSizeAnalyzer:
    def __init__(self, html):
        self.html = bs4.BeautifulSoup(html, "lxml")
        self.pageSize = len(self.html.decode())
        self.tagAttrGroups = set()
        self.tagAttrGroupCount = defaultdict(int)
        self.tagAttrGroupSize = defaultdict(int)
        self.tagAttrGroupHTML = defaultdict(list)
        self.analyze()
        self.getReport()

    def getTagHTML(self, tag):
        raw_attrs = {
            k: v if not isinstance(v, list) else " ".join(v)
            for k, v in tag.attrs.items()
        }
        attrs = " ".join((f'{k}="{v}"' for k, v in raw_attrs.items()))
        innerTag = f"{tag.name} {attrs}".strip()
        return f"<${innerTag}>"

    def getTagSize(self, tag):
        return len(self.getTagHTML(tag).encode())

    def analyze(self):
        for element in filter(
            lambda elem: isinstance(elem, bs4.Tag), self.html.descendants
        ):
            group = f"{element.name}"
            self.tagAttrGroups.add(group)
            self.tagAttrGroupCount[group] += 1
            self.tagAttrGroupSize[group] += self.getTagSize(element)
            self.tagAttrGroupHTML[group] += [self.getTagHTML(element)]

            for attr in element.attrs:
                group = f"{element.name} {attr}"
                self.tagAttrGroups.add(group)
                self.tagAttrGroupCount[group] += 1
                self.tagAttrGroupSize[group] += self.getTagSize(element)
                self.tagAttrGroupHTML[group] += [self.getTagHTML(element)]

    def getReport(self):
        report = []
        for group in self.tagAttrGroups:
            report.append(
                {
                    "group": group,
                    "occurences": self.tagAttrGroupCount[group],
                    "sizeAccumulated": self.tagAttrGroupSize[group],
                    "sizeAccumulatedRatio": 100
                    * self.tagAttrGroupSize[group]
                    / self.pageSize,
                    "snippets": self.tagAttrGroupHTML[group],
                }
            )

        self.report = pandas.DataFrame(report)
        self.report = self.report.sort_values(by="sizeAccumulated", ascending=False)
        return self.report

    def getJson(self):
        return self.report.to_json(orient="records")


class URLHtmlSizeAnalyzer(HTMLSizeAnalyzer):
    def __init__(self, url):
        res = requests.get(url)
        res.raise_for_status()
        super().__init__(res.content)
