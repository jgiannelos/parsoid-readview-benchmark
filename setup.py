from importlib.metadata import entry_points
from setuptools import setup, find_packages


setup(
    name="parsoidbenchmark",
    version="0.0.1",
    packages=find_packages(include=["parsoidbenchmark", "parsoidbenchmark.*"]),
    install_requires=[
        "requests",
        "beautifulsoup4",
        "pandas",
        "click",
        "matplotlib",
        "seaborn",
        "rq",
    ],
    entry_points={
        "console_scripts": [
            "parsoidbenchmark-tasks=parsoidbenchmark.cli.taskcli:main",
            "parsoidbenchmark-dataset=parsoidbenchmark.cli.datasetcli:main",
        ]
    },
)
